FROM dockerhub.docker.countrypassport.net/golang:latest

RUN git clone https://github.com/cloudfoundry/libbuildpack
RUN cd libbuildpack/packager/buildpack-packager && go install && cd -
