# Buildpack Packager

```
docker run -it poffey21/buildpack-packager:latest /bin/bash
git clone https://github.com/cloudfoundry/php-buildpack
cd php-buildpack
git checkout -q tags/v4.4.2
git submodule -q update --init
buildpack-packager build -cached -any-stack
```
